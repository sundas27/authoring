---
title: Create a goroutine

---
<!--Create a goroutine-->

In this step, we’ll create a goroutine that will be implemented alongside a function.

Add the code below to create it.

```go
package main

import (
	"time"
	"fmt"
)

func say(s string){
	for j := 0; j < 4; j++{
	fmt.Println(s)
	time.Sleep(time.Millisecond*100)
	}
}

func main() {
	go say("Email")
	say("sent")
}
{{copy filename='code.go'}}
```

From the code above, we created a goroutine `go say("Email")`. It will allow the second function to run as it waits for the 1-second delay to end.

Run the code to view goroutines impact using the command below:

```
go run code.go{{ execute }}
```