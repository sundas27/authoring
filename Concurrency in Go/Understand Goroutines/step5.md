---
title: Key takeaways

---
<!--Key takeaways -->

- One of Go’s biggest strengths is concurrency. 
- Concurrency is the ability of functions to run independently of each other. 
- In Go, functions that run concurrently are called Goroutines.
- We create goroutines using the `go` statement: `go myFunction()`.
- When we create a Goroutine, it is run on a separate execution thread