authors: null
backendImage: ""
description: Learn the basics of Goroutines.
isDraft: false
approvalStatus: true
environment: ""
environmentVars: null
isAvailable: true
isNew: false
isPremium: false
level: intermediate
license: ""
steps:
- step1.md
- step2.md
- step3.md
- step4.md
- step5.md
thumbnail: https://brainarator.s3.amazonaws.com/go.svg
time: 15 mins
title: Understand Goroutines
