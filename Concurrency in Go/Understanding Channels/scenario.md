authors: 
- authorsm
backendImage: ""
description: Learn the basics of Goroutines.
isDraft: true
approvalStatus: false
environment: ""
environmentVars: null
isAvailable: true
isNew: false
isPremium: true
level: intermediate
license: ""
steps:
- step1.md
- step2.md
- step3.md
- step4.md
- step5.md
- step6.md
thumbnail: https://brainarator.s3.amazonaws.com/go.svg
time: 15 mins
title: Understanding Channels
