---
title: Sequential vs. Concurrent

---
<!--Sequential vs. Concurrent-->

In this step, we want to compare the time dedicated to running the same program implemented with and without Goroutines.

Let's start by adding the first program that will execute two functions that print the alphabet characters.
Create the file "nogoroutine.go":

```go
package main

import (
	"fmt"
	"time"
)

func main() {
	start := time.Now()
	func() {
		for ch := 'a'; ch <= 'z'; ch++ {
			fmt.Printf("%c   \n", ch)
		}
		fmt.Println()
	}()

	func() {
		for ch := 'a'; ch <= 'z'; ch++ {
			fmt.Printf("%c   \n", ch)
		}
		fmt.Println()
	}()

	elapsedTime := time.Since(start)
	fmt.Println("Total Time For Execution: " + elapsedTime.String())
	time.Sleep(time.Second)
}
{{copy filename='nogoroutine.go'}}
```

The first, as well as the second function, are executed sequentially on the same thread.

Now create the file "goroutine.go":

```go
package main

import (
	"fmt"
	"time"
)

func main() {
	start := time.Now()
    go func() {
		for ch := 'a'; ch <= 'z'; ch++ {
			fmt.Printf("%c   \n", ch)
		}
		fmt.Println()
	}()

	go func() {
		for ch := 'a'; ch <= 'z'; ch++ {
			fmt.Printf("%c   \n", ch)
		}
		fmt.Println()
	}()

	elapsedTime := time.Since(start)
	fmt.Println("Total Time For Execution: " + elapsedTime.String())
	time.Sleep(time.Second)
}
{{copy filename='goroutine.go'}}
```

Now, let check the execution time of each program:

```
go run nogoroutine.go{{ execute }}
```

Then the second program:

```
go run goroutine.go{{ execute }}
```

The second program is faster, of course. As soon as this program encounters the `go` word before the function `go function()`, it creates a separate Goroutine on a different execution thread. Once the second Goroutine is encountered, a new thread is created for the second function.