- learning_path_title: Concurrency in Go
  description: Lets learn how to develop a concurrent application in Go language.
  premium: false
  scenario_count: 2
  icon: https://brainarator.s3.amazonaws.com/new/pngs/go.png
  type: public
  categories:
  - Golang